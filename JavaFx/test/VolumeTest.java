package sample;

import org.junit.Assert;
import org.junit.Test;
import sample.Volume;

public class VolumeTest {
    @Test
    public void volumeWorkerPass() {

        //given
        String inputValue = "1";
        String convertation1 = "l to m^3";
        String convertation2 = "l to gallon";
        String convertation3 = "l to pint";

        //when
        String testValue1 = Volume.volumeWorker(inputValue, convertation1);
        String testValue2 = Volume.volumeWorker(inputValue, convertation2);
        String testValue3 = Volume.volumeWorker(inputValue, convertation3);

        //then
        Assert.assertEquals(Double.toString(1 * 0.001), testValue1);
        Assert.assertEquals(Double.toString(1 * 0.2642), testValue2);
        Assert.assertEquals(Double.toString(1 * 2.113), testValue3);
    }

    @Test
    public void volumeWorkerFail() {

        //given
        String inputValue = "60";
        String convertation = "";

        //when
        String testValue = Volume.volumeWorker(inputValue, convertation);

        //then
        Assert.assertNotEquals("1.0", testValue);

    }
}
