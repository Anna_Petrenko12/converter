package sample;

import org.junit.Assert;
import org.junit.Test;
import sample.Temperature;

public class TemperatureTest {
    @Test
    public void temperatureWorkerPass() {

        //given
        String inputValue = "1";
        String convertation1 = "C to K";
        String convertation2 = "C to F";
        String convertation3 = "C to Re";

        //when
        String testValue1 = Temperature.temperatureWorker(inputValue, convertation1);
        String testValue2 = Temperature.temperatureWorker(inputValue, convertation2);
        String testValue3 = Temperature.temperatureWorker(inputValue, convertation3);

        //then
        Assert.assertEquals(Double.toString(1 + 273.15), testValue1);
        Assert.assertEquals(Double.toString(1 * (9 / 5) + 32), testValue2);
        Assert.assertEquals(Double.toString(1 * (4 / 5)), testValue3);
    }

    @Test
    public void temperatureWorkerFail() {
        //given
        String inputValue = "60";
        String convertation = "";

        //when
        String testValue = Temperature.temperatureWorker(inputValue, convertation);

        //then
        Assert.assertNotEquals("1.0", testValue);

    }
}
