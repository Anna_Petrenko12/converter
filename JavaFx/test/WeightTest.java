package sample;

import org.junit.Assert;
import org.junit.Test;
import sample.Weight;

public class WeightTest {
    @Test
    public void weightWorkerPass() {

        //given
        String inputValue = "1";
        String convertation1 = "kg to g";
        String convertation2 = "kg to c";
        String convertation3 = "kg to carat";

        //when
        String testValue1 = Weight.weightWorker(inputValue, convertation1);
        String testValue2 = Weight.weightWorker(inputValue, convertation2);
        String testValue3 = Weight.weightWorker(inputValue, convertation3);

        //then
        Assert.assertEquals(Double.toString(1 * 1000), testValue1);
        Assert.assertEquals(Double.toString(1 * 100), testValue2);
        Assert.assertEquals(Double.toString(1 * 5000), testValue3);
    }

    @Test
    public void weightWorkerFail() {
        //given
        String inputValue = "60";
        String convertation = "";

        //when
        String testValue = Weight.weightWorker(inputValue, convertation);

        //then
        Assert.assertNotEquals("1.0", testValue);

    }
}
