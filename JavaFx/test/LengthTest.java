package sample;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class LengthTest {
    @Test
    public void lengthWorkerPass() {

        //given
        String inputValue = "1";
        String convertation1 = "m to km";
        String convertation2 = "m to mile";
        String convertation3 = "m to nautical mile";

        //when
        String testValue1 = Length.lengthWorker(inputValue, convertation1);
        String testValue2 = Length.lengthWorker(inputValue, convertation2);
        String testValue3 = Length.lengthWorker(inputValue, convertation3);

        //then
        Assert.assertEquals(Double.toString(1 * 0.001), testValue1);
        Assert.assertEquals(Double.toString(1 * 0.000621371192), testValue2);
        Assert.assertEquals(Double.toString(1 * 0.000539956803), testValue3);
    }

    @Test
    public void lengthWorkerFail() {
        //given
        String inputValue = "60";
        String convertation = "";

        //when
        String testValue = Length.lengthWorker(inputValue, convertation);

        //then
        Assert.assertNotEquals("1.0", testValue);

    }
}