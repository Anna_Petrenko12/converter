package sample;

import org.junit.Assert;
import org.junit.Test;
import sample.Time;

public class TimeTest {
    @Test
    public void timeWorkerPass() {

        //given
        String inputValue = "60";
        String convertation1 = "Seconds to Minutes";
        String convertation2 = "Seconds to Hour";
        String convertation3 = "Seconds to Day";

        //when
        String testValue1 = Time.timeWorker(inputValue, convertation1);
        String testValue2 = Time.timeWorker(inputValue, convertation2);
        String testValue3 = Time.timeWorker(inputValue, convertation3);

        //then
        Assert.assertEquals(Double.toString(60 / 60), testValue1);
        Assert.assertEquals(Double.toString(60 / 3600), testValue2);
        Assert.assertEquals(Double.toString(60 / 86400), testValue3);
    }

    @Test
    public void timeWorkerFail() {
        //given
        String inputValue = "60";
        String convertation = "";

        //when
        String testValue = Time.timeWorker(inputValue, convertation);

        //then
        Assert.assertNotEquals("1.0", testValue);

    }
}
