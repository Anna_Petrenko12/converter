package sample;

public class Weight {
    public static String weightWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);


        switch (convertation) {
            case "kg to g":
                result = Double.toString(value * 1000);
                break;
            case "kg to c":
                result = Double.toString(value * 100);
                break;
            case "kg to carat":
                result = Double.toString(value * 5000);
                break;
            case "kg to eng pound":
                result = Double.toString(value / 0.45359237);
                break;
            case "kg to pound":
                result = Double.toString(value * 0.40951241);
                break;
            case "kg to stone":
                result = Double.toString(value * 0.157473044);
                break;
            case "g to kg":
                result = Double.toString(value / 1000);
                break;
            case "c to kg":
                result = Double.toString(value / 100);
                break;
            case "carat to kg":
                result = Double.toString(value / 5000);
                break;
            case "eng pound to kg":
                result = Double.toString(value * 0.45359237);
                break;
            case "pound to kg":
                result = Double.toString(value / 0.40951241);
                break;
            case "stone to kg":
                result = Double.toString(value / 0.157473044);
                break;
            default:
                result = "Choose convertation";
        }
        return result;
    } catch (Exception e) {
        result = "Enter only numbers!";
        return result;
    }
    }
}
