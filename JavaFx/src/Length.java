package sample;

public class Length {

    public static String lengthWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
            switch (convertation) {
                case "m to km":
                    result = Double.toString(value * 0.001);
                    break;
                case "m to mile":
                    result = Double.toString(value * 0.000621371192);
                    break;
                case "m to nautical mile":
                    result = Double.toString(value * 0.000539956803);
                    break;
                case "m to cable":
                    result = Double.toString(value * 0.00455672208);
                    break;
                case "m to league":
                    result = Double.toString(value * 0.000179985601);
                    break;
                case "m to foot":
                    result = Double.toString(value * 3.2808399);
                    break;
                case "m to yard":
                    result = Double.toString(value * 1.0936133);
                    break;
                case "km to m":
                    result = Double.toString(value / 0.001);
                    break;
                case "mile to m":
                    result = Double.toString(value / 0.000621371192);
                    break;
                case "nautical mile to m":
                    result = Double.toString(value / 0.000539956803);
                    break;
                case "cable to m":
                    result = Double.toString(value / 0.00455672208);
                    break;
                case "league to m":
                    result = Double.toString(value / 0.000179985601);
                    break;
                case "foot to m":
                    result = Double.toString(value / 3.2808399);
                    break;
                case "yard to m":
                    result = Double.toString(value / 1.0936133);
                    break;
                default:
                    result = "Choose convertation";
            }
            return result;
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }
    }
}
