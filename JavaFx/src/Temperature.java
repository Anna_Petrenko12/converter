package sample;

public class Temperature {
    public static String temperatureWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);


        switch (convertation) {
            case "C to K":
                result = Double.toString(value + 273.15);
                break;
            case "C to F":
                result = Double.toString(value * (9 / 5) + 32);
                break;
            case "C to Re":
                result = Double.toString(value * (4 / 5));
                break;
            case "C to Ro":
                result = Double.toString(value * (21 / 40) + 7.5);
                break;
            case "C to Ra":
                result = Double.toString((value + 273.15) * (9 / 5));
                break;
            case "C to N":
                result = Double.toString(value * 0.33);
                break;
            case "C to D":
                result = Double.toString((100 - value) * (3 / 2));
                break;
            case "K to C":
                result = Double.toString(value - 273.15);
                break;
            case "F to C":
                result = Double.toString((value - 32) * (5 / 9));
                break;
            case "Re to C":
                result = Double.toString(value * (5 / 4));
                break;
            case "Ro to C":
                result = Double.toString((value - 7.5) * (40 / 21));
                break;
            case "Ra to C":
                result = Double.toString((value - 491.67) * (5 / 9));
                break;
            case "N to C":
                result = Double.toString(value * (100 / 33));
                break;
            case "D to C":
                result = Double.toString((100 - value) * (2 / 3));
                break;
            default:
                result = "Choose convertation";
        }
        return result;
    } catch (Exception e) {
        result = "Enter only numbers!";
        return result;
    }
    }
}
