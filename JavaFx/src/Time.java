package sample;


public class Time {
    public static String timeWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);


        switch (convertation) {
            case "Seconds to Minutes":
                result = Double.toString(value / 60);
                break;
            case "Seconds to Hour":
                result = Double.toString(value / 3600);
                break;
            case "Seconds to Day":
                result = Double.toString(value / 86400);
                break;
            case "Seconds to Week":
                result = Double.toString(value / 604800);
                break;
            case "Seconds to Month":
                result = Double.toString(value / 2419200);
                break;
            case "Seconds to Astronomical Year":
                result = Double.toString(value / 31536000);
                break;
            case "Seconds to Third":
                result = Double.toString(value / 60);
                break;
            case "Minutes to Seconds":
                result = Double.toString(value * 60);
                break;
            case "Hour to Seconds":
                result = Double.toString(value * 3600);
                break;
            case "Day to Seconds":
                result = Double.toString(value * 86400);
                break;
            case "Week to Seconds":
                result = Double.toString(value * 604800);
                break;
            case "Month to Seconds":
                result = Double.toString(value * 2419200);
                break;
            case "Astronomical Year to Seconds":
                result = Double.toString(value * 31536000);
                break;
            case "Third to Seconds":
                result = Double.toString(value * 60);
                break;
            default:
                result = "Choose convertation";
        }
        return result;
    } catch (Exception e) {
        result = "Enter only numbers!";
        return result;
    }
    }
}
