package sample;

public class Volume {


    public static String volumeWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);

        switch (convertation) {
            case "l to m^3":
                result = Double.toString(value * 0.001);
                break;
            case "l to gallon":
                result = Double.toString(value * 0.2642);
                break;
            case "l to pint":
                result = Double.toString(value * 2.113);
                break;
            case "l to quart":
                result = Double.toString(value * 1.057);
                break;
            case "l to barrel":
                result = Double.toString(value * 0.00629);
                break;
            case "l to cubic foot":
                result = Double.toString(value * 0.0353146667);
                break;
            case "l to cubic inch":
                result = Double.toString(value * 61.0237441);
                break;
            case "m^3 to l":
                result = Double.toString(value / 0.001);
                break;
            case "gallon to l":
                result = Double.toString(value / 0.2642);
                break;
            case "pint to l":
                result = Double.toString(value / 2.113);
                break;
            case "quart to l":
                result = Double.toString(value / 1.057);
                break;
            case "barrel to l":
                result = Double.toString(value / 0.00629);
                break;
            case "cubic foot to l":
                result = Double.toString(value / 0.0353146667);
                break;
            case "cubic inch to l":
                result = Double.toString(value / 61.0237441);
                break;
            default:
                result = "Choose convertation";
        }
        return result;
    }
        catch (Exception e) {
        result = "Enter only numbers!";
        return result;
    }
    }
}
