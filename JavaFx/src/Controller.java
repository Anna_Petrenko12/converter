package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {

    @FXML
    private RadioButton lengthRadioBtn;

    @FXML
    private RadioButton temperatureRadioBtn;

    @FXML
    private RadioButton weightRadioBtn;

    @FXML
    private RadioButton timeRadioBtn;

    @FXML
    private RadioButton volumeRadioBtn;

    @FXML
    private Button convertBtn;

    @FXML
    private TextField enterNumberField;

    @FXML
    private ComboBox<String> listConvertation;

    @FXML
    private TextField resultField;

    @FXML
    private Label lab;

    ToggleGroup group;

    @FXML
    void initialize() {
        group = new ToggleGroup();
        weightRadioBtn.setToggleGroup(group);
        timeRadioBtn.setToggleGroup(group);
        volumeRadioBtn.setToggleGroup(group);
        lengthRadioBtn.setToggleGroup(group);
        temperatureRadioBtn.setToggleGroup(group);
    }


    @FXML
    void handleLengthBtn(ActionEvent event) {
        ObservableList<String> langs = FXCollections.observableArrayList("m to km", "m to mile", "m to nautical mile", "m to cable", "m to league", "m to foot", "m to yard", "km to m", "mile to m", "nautical mile to m", "cable to m", "league to m", "foot to m", "yard to m");
        listConvertation.setItems(langs);


    }

    @FXML
    void handleTemperatureBtn(ActionEvent event) {
        ObservableList<String> langs = FXCollections.observableArrayList("C to K", "C to F", "C to Re", "C to Ro", "C to Ra", "C to N", "C to D", "K to C", "F to C", "Re to C", "Ro to C", "Ra to C", "N to C", "D to C");
        listConvertation.setItems(langs);

    }

    @FXML
    void handleTimeBtn(ActionEvent event) {
        ObservableList<String> langs = FXCollections.observableArrayList("Seconds to Minutes", "Seconds to Hour", "Seconds to Day", "Seconds to Week", "Seconds to Month", "Seconds to Astronomical Year", "Seconds to Third", "Minutes to Seconds", "Hour to Seconds", "Day to Seconds", "Week to Seconds", "Month to Seconds", "Astronomical Year to Seconds", "Third to Seconds");
        listConvertation.setItems(langs);

    }

    @FXML
    void handleVolumeBtn(ActionEvent event) {
        ObservableList<String> langs = FXCollections.observableArrayList("l to m^3", "l to gallon", "l to pint", "l to quart", "l to barrel", "l to cubic foot", "l to cubic inch", "m^3 to l", "gallon to l", "pint to l", "quart to l", "barrel to l", "cubic foot to l", "cubic inch to l");
        listConvertation.setItems(langs);

    }

    @FXML
    void handleWeightBtn(ActionEvent event) {
        ObservableList<String> langs = FXCollections.observableArrayList("kg to g", "kg to c", "kg to carat", "kg to eng pound", "kg to pound", "kg to stone", "g to kg", "c to kg", "carat to kg", "eng pound to kg", "pound to kg", "stone to kg");
        listConvertation.setItems(langs);

    }

    @FXML
    void handleConvertBtn(ActionEvent event) {
        RadioButton selected = (RadioButton) group.getSelectedToggle();
        String inputNumber = enterNumberField.getText();
        try {

            if (selected.getText().equals("Time")) {

                try {
                    lab.setText(Time.timeWorker(inputNumber, listConvertation.getValue()));
                } catch (Exception o) {
                    lab.setText("Choose convertation");
                }

            } else if (selected.getText().equals("Length")) {

                try {
                    lab.setText(Length.lengthWorker(inputNumber, listConvertation.getValue()));
                } catch (Exception o) {
                    lab.setText("Choose convertation");
                }

            } else if (selected.getText().equals("Weight")) {

                try {
                    lab.setText(Weight.weightWorker(inputNumber, listConvertation.getValue()));
                } catch (Exception o) {
                    lab.setText("Choose convertation");
                }

            } else if (selected.getText().equals("Volume")) {

                try {
                    lab.setText(Volume.volumeWorker(inputNumber, listConvertation.getValue()));
                } catch (Exception o) {
                    lab.setText("Choose convertation");
                }

            } else if (selected.getText().equals("Temperature")) {
                lab.setText(Temperature.temperatureWorker(inputNumber, listConvertation.getValue()));
            }

        } catch (Exception e) {
            lab.setText("Select Length, Temperature, Weight, Time or Volume");
        }


    }
}