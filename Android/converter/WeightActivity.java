package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class WeightActivity extends AppCompatActivity {
    TextView textName;
    EditText txtKg,txtFromConverte;
    Button btnFrom,btnKg,reset;
    Spinner spinnerTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textName=findViewById(R.id.textName);
        txtKg=findViewById(R.id.txtKg);
        txtFromConverte=findViewById(R.id.txtFromConverte);
        spinnerTo=findViewById(R.id.spinnerTo);
        btnFrom=findViewById(R.id.btnFrom);
        btnKg=findViewById(R.id.btnKg);
        reset=findViewById(R.id.reset);

        String[] to={"g","c","carat","eng pound","poung","stone","rus pound"};
        ArrayAdapter ad1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,to);
        spinnerTo.setAdapter(ad1);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertKgAtAnotherWeight();
                btnKg.setEnabled(false);
            }
        });
        btnKg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertWeightToKg();
                btnFrom.setEnabled(false);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnKg.setEnabled(true);
                btnFrom.setEnabled(true);
                txtKg.setText(" ");
                txtFromConverte.setText(" ");
            }
        });
    }

    private void convertWeightToKg() {
        Double converter = null;

        Double time = Double.parseDouble(txtKg.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="g"){
            converter=time/1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="c"){
            converter=time/100;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="carat"){
            converter=time/5000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="eng pound"){
            converter=time/0.4535923745 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="poung"){
            converter=time/2.2046;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="stone"){
            converter=time/0.15747;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="rus pound"){
            converter=time/0.40951203079;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnFrom.setEnabled(false);
        txtKg.setText(String.valueOf(converter));

    }

    private void convertKgAtAnotherWeight() {
        Double converter = null;

        Double time = Double.parseDouble(txtKg.getText().toString());


        if(spinnerTo.getSelectedItem().toString() =="g"){
            converter=time*1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="c"){
            converter=time*100;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="carat"){
            converter=time*5000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="eng pound"){
            converter=time*0.4535923745 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="poung"){
            converter=time*2.2046;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="stone"){
            converter=time*0.15747;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="rus pound"){
            converter=time*0.40951203079;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnKg.setEnabled(false);
        txtFromConverte.setText(String.valueOf(converter));
    }
}