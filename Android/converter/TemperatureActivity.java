package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TemperatureActivity extends AppCompatActivity {

    TextView textName;
    EditText txtC,txtFromConverte;
    Button btnFrom,btnC,reset;
    Spinner spinnerTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textName=findViewById(R.id.textName);
        txtC=findViewById(R.id.txtKg);
        txtFromConverte=findViewById(R.id.txtFromConverte);
        spinnerTo=findViewById(R.id.spinnerTo);
        btnFrom=findViewById(R.id.btnFrom);
        btnC=findViewById(R.id.btnKg);
        reset=findViewById(R.id.reset);

        String[] to={"k","F","Re","Ro","Ra","N"};
        ArrayAdapter ad1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,to);
        spinnerTo.setAdapter(ad1);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertCelsiusAtAnotherTemperature();
                btnC.setEnabled(false);
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertTemperatureCelsius();
                btnFrom.setEnabled(false);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnC.setEnabled(true);
                btnFrom.setEnabled(true);
                txtC.setText(" ");
                txtFromConverte.setText(" ");
            }
        });
    }

    private void convertTemperatureCelsius() {
        Double converter = null;

        Double time = Double.parseDouble(txtC.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="k"){
            converter=time-273.15;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="F"){
            converter=(time-32)/1.8000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Re"){
            converter=time/0.80000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Ro"){
            converter=(time-7.5)/0.52500 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Ra"){
            converter=(time-491.67)/1.80000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="N"){
            converter=time/0.33000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }

        btnFrom.setEnabled(false);
        txtC.setText(String.valueOf(converter));

    }

    private void convertCelsiusAtAnotherTemperature() {
        Double converter = null;

        Double time = Double.parseDouble(txtC.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="k"){
            converter=time+ 274.1500;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="F"){
            converter=time*33.80000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Re"){
            converter=time*0.8;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Ro"){
            converter=time*0.52500 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Ra"){
            converter=time*1.8000+ 491.67;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="N"){
            converter=time*0.3300000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnC.setEnabled(false);
        txtFromConverte.setText(String.valueOf(converter));
    }
}