package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class VolumeActivity extends AppCompatActivity {

    TextView textName;
    EditText txtL,txtFromConverte;
    Button btnFrom,btnL,reset;
    Spinner spinnerTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textName=findViewById(R.id.textName);
        txtL=findViewById(R.id.txtKg);
        txtFromConverte=findViewById(R.id.txtFromConverte);
        spinnerTo=findViewById(R.id.spinnerTo);
        btnFrom=findViewById(R.id.btnFrom);
        btnL=findViewById(R.id.btnKg);
        reset=findViewById(R.id.reset);

        String[] to={"m^3","gallon","pint","quart","barrel","cubic foot","cubic inch"};
        ArrayAdapter ad1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,to);
        spinnerTo.setAdapter(ad1);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertLiterAtAnotherVolume();
                btnL.setEnabled(false);
            }
        });
        btnL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertVolumeToLiter();
                btnFrom.setEnabled(false);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnL.setEnabled(true);
                btnFrom.setEnabled(true);
                txtL.setText(" ");
                txtFromConverte.setText(" ");
            }
        });
    }

    private void convertVolumeToLiter() {
        Double converter = null;

        Double volume = Double.parseDouble(txtL.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="m^3"){
            converter=volume*1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="gallon"){
            converter=volume/0.21997;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="pint"){
            converter=volume/1.7598;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="quart"){
            converter=volume/1.0567 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="barrel"){
            converter=volume/0.0062898;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cubic foot"){
            converter=volume/0.035315;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cubic inch"){
            converter=volume*61.024;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnFrom.setEnabled(false);
        txtL.setText(String.valueOf(converter));

    }

    private void convertLiterAtAnotherVolume() {
        Double converter = null;

        Double volume = Double.parseDouble(txtL.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="m^3"){
            converter=volume*1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="gallon"){
            converter=volume*0.21997;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="pint"){
            converter=volume*1.7598;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="quart"){
            converter=volume*1.0567 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="barrel"){
            converter=volume*0.0062898;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cubic foot"){
            converter=volume*0.035315;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cubic inch"){
            converter=volume*61.024;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnL.setEnabled(false);
        txtFromConverte.setText(String.valueOf(converter));
    }
}