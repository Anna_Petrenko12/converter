package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LengthActivity extends AppCompatActivity {

    TextView textName;
    EditText txtMeter,txtFromConverte;
    Button btnFrom,btnMeter,reset;
    Spinner spinnerTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textName=findViewById(R.id.textName);
        txtMeter=findViewById(R.id.txtKg);
        txtFromConverte=findViewById(R.id.txtFromConverte);
        spinnerTo=findViewById(R.id.spinnerTo);
        btnFrom=findViewById(R.id.btnFrom);
        btnMeter=findViewById(R.id.btnKg);
        reset=findViewById(R.id.reset);

        String[] to={"km","mile","nautical mile","cable","league","foot","yard"};
        ArrayAdapter ad1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,to);
        spinnerTo.setAdapter(ad1);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertMeterAtAnotherLength();
                btnMeter.setEnabled(false);
            }
        });
        btnMeter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertLengthToMeter();
                btnFrom.setEnabled(false);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMeter.setEnabled(true);
                btnFrom.setEnabled(true);
                txtMeter.setText(" ");
                txtFromConverte.setText(" ");
            }
        });
    }

    private void convertLengthToMeter() {
        Double converter = null;

        Double time = Double.parseDouble(txtMeter.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="km"){
            converter=time*1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="mile"){
            converter=time*1609.344;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="nautical mile"){
            converter=time*1852.3;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cable"){
            converter=time*000 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="league"){
            converter=time*4828.04;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="foot"){
            converter=time*3.2808;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="yard"){
            converter=time*0.9144;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnFrom.setEnabled(false);
        txtMeter.setText(String.valueOf(converter));

    }

    private void convertMeterAtAnotherLength() {
        Double converter = null;

        Double time = Double.parseDouble(txtMeter.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="km"){
            converter=time/1000;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="mile"){
            converter=time/1609.344;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="nautical mile"){
            converter=time/1852.3;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="cable"){
            converter=time/000 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="league"){
            converter=time/4828.04;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="foot"){
            converter=time/3.2808;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="yard"){
            converter=time*0.9144;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnMeter.setEnabled(false);
        txtFromConverte.setText(String.valueOf(converter));
    }
}