package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TimeActivity extends AppCompatActivity {
    TextView textName;
    EditText txtSeconds,txtFromConverte;
    Button btnFrom,btnSecond,reset;
    Spinner spinnerTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textName=findViewById(R.id.textName);
        txtSeconds=findViewById(R.id.txtKg);
        txtFromConverte=findViewById(R.id.txtFromConverte);
        spinnerTo=findViewById(R.id.spinnerTo);
        btnFrom=findViewById(R.id.btnFrom);
        btnSecond=findViewById(R.id.btnKg);
        reset=findViewById(R.id.reset);

        String[] to={"Min","Hour","Day","Week","Month","Astronomical Year"};
        ArrayAdapter ad1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,to);
        spinnerTo.setAdapter(ad1);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertSecondAtAnotherTime();
                btnSecond.setEnabled(false);
            }
        });
        btnSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSecond.setEnabled(false);
                convertTimeToSecond();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSecond.setEnabled(true);
                btnFrom.setEnabled(true);
                txtSeconds.setText("");
                txtFromConverte.setText("");
            }
        });
}

    private void convertTimeToSecond() {
        Double converter = null;

        Double time = Double.parseDouble(txtSeconds.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="Min"){
            converter=time*60;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Hour"){
            converter=time*3600;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Day"){
            converter=time*86400;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Week"){
            converter=time*604800 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Month"){
            converter=time*2629743.83;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Astronomical Year"){
            converter=time*31556952;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnFrom.setEnabled(false);
        txtSeconds.setText(String.valueOf(converter));

    }

    private void convertSecondAtAnotherTime() {
        Double converter = null;

        Double time = Double.parseDouble(txtSeconds.getText().toString());

        if(spinnerTo.getSelectedItem().toString() =="Min"){
            converter=time/60;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Hour"){
            converter=time/3600;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Day"){
            converter=time/86400;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Week"){
            converter=time/604800 ;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Month"){
            converter=time/2629743.83;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        else  if(spinnerTo.getSelectedItem().toString() =="Astronomical Year"){
            converter=time/31556952;
            Toast.makeText(getApplicationContext(),converter.toString(), Toast.LENGTH_SHORT).show();
        }
        btnSecond.setEnabled(false);
        txtFromConverte.setText(String.valueOf(converter));
    }
    }
