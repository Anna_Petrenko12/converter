package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnTime,btnVolume,btnLength,btnTemperature,btnWeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTime=(Button)findViewById(R.id.btnTime);
        btnTime.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                openTimeActivity();
            }
        });
        btnVolume=(Button)findViewById(R.id.btnVolume);
        btnVolume.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                openVolumeActivity();
            }
        });
        btnLength=(Button)findViewById(R.id.btnLength);
        btnLength.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                openLengthActivity();
            }
        });
        btnTemperature=(Button)findViewById(R.id.btnTemperature);
        btnTemperature.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                openTemperatureActivity();
            }
        });
        btnWeight=(Button)findViewById(R.id.btnWeight);
        btnWeight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                openWeightActivity();
            }
        });

    }

    private void openTemperatureActivity() {
        Intent temperature = new Intent(this, TemperatureActivity.class);
        startActivity(temperature);
    }
    private void openWeightActivity() {
        Intent weight = new Intent(this, WeightActivity.class);
        startActivity(weight);
    }
    private void openLengthActivity() {
        Intent length = new Intent(this, LengthActivity.class);
        startActivity(length);
    }

    private void openTimeActivity() {
        Intent time = new Intent(this, TimeActivity.class);
        startActivity(time);
    }
    private void openVolumeActivity() {
        Intent volume = new Intent(this, VolumeActivity.class);
        startActivity(volume);
    }
    }

